var SuperSprite = class {
    constructor(name, superSpriteManager) {
        this.superSpriteManager = superSpriteManager;
        this.mesh = superSpriteManager._addSprite(name);
    }

    get position() {
        return this.mesh.position;
    }

    get material() {
        return this.mesh.material;
    }

    playAnimation(startFrame, endFrame, loop, delay) {
        var currentFrame = startFrame;
        this.superSpriteManager._setFrame(currentFrame);
        var interval = setInterval(((obj) => {
            return () => {
                if (currentFrame === endFrame) {
                    if (loop) {
                        currentFrame = startFrame;
                    } else {
                        clearInterval(interval);
                        return;
                    }
                }
                currentFrame++;
                obj.superSpriteManager._setFrame(currentFrame);
            }
        })(this), delay);
    }
};

var SuperSpriteManager = class {
    constructor(name, diffuseName, maxBillboards, cellSize, scene) {
        this.UV_FACTOR = .0026;

        this.numSprites = 0;
        this.maxBillboards = maxBillboards;

        // check type
        if (typeof cellSize === 'number') {
            this.cellHeight = cellSize;
            this.cellWidth = cellSize;
        } else if (typeof cellSize === 'object'
            && 'width' in cellSize
            && 'height' in cellSize
        ) {
            this.cellHeight = cellSize['height'];
            this.cellWidth = cellSize['width'];
        } else {
            throw "Wrong cellSize argument!";
        }

        this.material = new BABYLON.StandardMaterial("", scene);
        this.material.diffuseTexture = new BABYLON.Texture(diffuseName, scene);
        this.material.specularColor = new BABYLON.Color3(1, 1, 1);
        this.material.diffuseTexture.hasAlpha = true;
        this.material.backFaceCulling = false;
    }

    _addSprite(name) {
        if (this.numSprites > this.maxBillboards) {
            throw "Max SuperSprites reached!";
        }

        this.numSprites++;

        if (this.original === undefined) {
            this.original = BABYLON.MeshBuilder.CreatePlane(name, { size: 1 }, scene);

            var setSize = function(obj) {
                // BUG! See http://www.html5gamedevs.com/topic/13642-get-texture-size/
                let size = obj.material.diffuseTexture.getBaseSize();
                if (size.width === 0) {
                    setTimeout(() => setSize(obj), 100);
                    return;
                }
                obj.material.diffuseTexture.wrapU = BABYLON.Texture.CLAMP_ADDRESSMODE;
                obj.material.diffuseTexture.wrapV = BABYLON.Texture.CLAMP_ADDRESSMODE;

                if (size.height != obj.cellHeight) {
                    obj.material.diffuseTexture.vOffset += obj.cellHeight * obj.UV_FACTOR * 2;
                }

                obj.material.diffuseTexture.uScale = obj.cellWidth / size.width; 
                obj.material.diffuseTexture.vScale = obj.cellHeight / size.height;
            }

            setSize(this);

            this.original.material = this.material;
            this.original.billboardMode = BABYLON.AbstractMesh.BILLBOARDMODE_ALL;
            this.original.position.y += .5;

            return this.original;
        } else {
            var newClone = this.original.clone(name);
            return newClone;
        }
    }

    _setFrame(frame) {
        var doSetFrame = (obj) => {
            let size = obj.material.diffuseTexture.getBaseSize();
            let numXFrames = size.width / obj.cellWidth;
            let numFrames = numXFrames * size.height / obj.cellWidth;

            if (frame < 0 || frame > numFrames) {
                // error
                return;
            }

            obj.material.diffuseTexture.uOffset = obj.cellWidth * .00071 * (frame % numXFrames);
            obj.material.diffuseTexture.vOffset = (obj.cellHeight * obj.UV_FACTOR * 2) + obj.cellHeight * obj.UV_FACTOR * 2 * Math.floor(frame / numXFrames);
        };

        if (this.material.diffuseTexture.getBaseSize().width === 0) {
            setTimeout(() => doSetFrame(this), 100);
        } else {
            doSetFrame(this);
        }
    }
};
